# Strings are used to display message's on the screen or to convey instruction to users
test = 'This is a testing.'
print(test)
# Now say for example you want to print 'You're having it' if we write in single quotes then it will show us error
# test1 = 'You're having it' # uncomment the line to see error on this line
# so for this error we can use double quotes to solve this error
test2 = "You're having it"  # so as you can see in test1 we are getting error but now if you print it. it will work.
print(test2)

# now this are few examples of single and double quotes
print('Python Tutorial "Beginners"')
print("Python Tutorial Beginner's")
